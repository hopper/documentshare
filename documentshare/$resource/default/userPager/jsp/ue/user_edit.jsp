<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="net.simpleframework.util.StringUtils"%>
<%@ page import="net.simpleframework.organization.OrgUtils"%>
<%@ page import="net.simpleframework.organization.component.userpager.UserPagerUtils"%>
<%@ page import="net.simpleframework.web.page.component.ComponentParameter"%>
<form id="__user_edit_form"><input type="hidden"
	id="<%=OrgUtils.um().getUserIdParameterName()%>" name="<%=OrgUtils.um().getUserIdParameterName()%>"
	value="<%=StringUtils.blank(request.getParameter(OrgUtils.um()
					.getUserIdParameterName()))%>" /></form>
<div id="__user_edit"></div>
<script type="text/javascript">
	function __user_pager_refresh() {
	<%final ComponentParameter nComponentParameter = UserPagerUtils
					.getComponentParameter(request, response);
			if (nComponentParameter.componentBean != null) {%>
	$Actions["<%=nComponentParameter.getBeanProperty("name")%>"].refresh();
	<%}%>
	}
</script>