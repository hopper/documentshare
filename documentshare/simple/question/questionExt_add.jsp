<%@page
	import="net.simpleframework.web.page.component.ComponentParameter"%>
<%@page
	import="net.simpleframework.web.page.component.ComponentRenderUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="net.simpleframework.web.page.PageRequestResponse"%><%@page
	import="net.documentshare.question.QuestionUtils"%>

<%
	final ComponentParameter nComponentParameter = new ComponentParameter(request, response, null);
%>
<div id="questionExtForm" class="simple_toolbar2">
	<input type="hidden" id="questionId" name="questionId"
		value="<%=nComponentParameter.getRequestParameter(QuestionUtils.questionId)%>">
	<table cellspacing="0" style="width: 100%;">
		<tr>
			<td style="padding: 3px;">
				<div>
					<textarea id="qExt_content" name="qExt_content" rows="12"
						style="width: 98%;"></textarea>
				</div>
			</td>
		</tr>
	</table>
	<table cellspacing="0">
		<tr>
			<td>
				<input type="button" class="button2" value="保存"
					onclick="$IT.A('questionExtAddAct')" id="__questionExtAdd" />
				<input type="button" value="取消"
					onclick="$IT.C('reAddQuestionExtWindowAct')" />
			</td>
		</tr>
	</table>
</div>
