<%@page import="net.simpleframework.ado.db.IQueryEntitySet"%>
<%@page import="net.simpleframework.organization.account.AccountSession"%>
<%@page import="net.simpleframework.organization.account.IAccount"%>
<%@page import="net.simpleframework.util.ConvertUtils"%>
<%@page import="net.simpleframework.web.page.PageRequestResponse"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="net.simpleframework.util.HTMLUtils"%>
<%@page import="net.simpleframework.util.HTMLBuilder"%><%@page
	import="net.documentshare.question.QuestionUtils"%><%@page
	import="net.documentshare.question.QuestionBean"%><%@page
	import="net.documentshare.impl.AbstractAttention"%><%@page
	import="net.documentshare.utils.ItSiteUtil"%>
<%
	final PageRequestResponse requestResponse = new PageRequestResponse(request, response);
	final QuestionBean questionBean = QuestionUtils.applicationModule.getViewQuestionBean(requestResponse);
	if (questionBean == null) {
		out.println("该问答无法查询!");
		return;
	}
	final IAccount account = AccountSession.getLogin(requestResponse.getSession());
	AbstractAttention csAttention = null;
	if (account != null)
		csAttention = QuestionUtils.applicationModule.getAttention(account.getId(), questionBean.getId());
%>
<head>
	<style>
.m_dop {
	height: 33px;
	text-align: center;
	background: #F1F4FC;
	padding-top: 10px;
}
</style>
</head>
<div class="show_template">
	<div class="nav2 clear_float">
		<div id="act_51346" style="float: right;">
			<%
				if (account != null) {
			%>
			<a class="a2" id="__question_attention"
				onclick="$Actions['question_attentionAct']('questionId=<%=questionBean.getId()%>');"><%=(csAttention != null ? "取消关注" : "关注")%></a>(
			<a class="a2" id="__attentionUsersWindow"
				onclick="$Actions['attentionUsersWindowAct']('questionId=<%=questionBean.getId()%>');"><%=questionBean.getAttentions()%></a>)
			<%
				if (ItSiteUtil.isManageOrSelf(requestResponse, QuestionUtils.applicationModule, questionBean.getUserId())) {
			%>
			<span style="margin: 0px 4px;">|</span><a class="a2"
				onclick="$Actions['questionAddWindowAct']('questionId=<%=questionBean.getId()%>');">修改</a>
			<%
				}
				}
			%>
		</div>
		<div style="float: left;">
			<div class="nav1_image">
				<a href="/question.html">问答</a><%=HTMLBuilder.NAV%><%=questionBean.getTitle()%>
			</div>
		</div>
	</div>
	<div class="sj">
		<div class="f2" style="padding-bottom: 8px;">
			<%
				out.print(questionBean.getTitle());
			%>
			<div class='voteCommon'>
				<%=ItSiteUtil.buildVote(questionBean, "questionId")%>
			</div>
		</div>
		<div class="clear_float">
			<div style="float: right;"><%=questionBean.getRemarks()%>评/<%=questionBean.getViews()%>阅
			</div>
			<div style="float: left;">
				<span><%=QuestionUtils.formatQuestionHeader(requestResponse, questionBean)%></span><span
					style="margin-left: 15px;"></span>
			</div>
		</div>
		<%
			if (ItSiteUtil.isManageOrSelf(requestResponse, QuestionUtils.applicationModule, questionBean.getUserId()) && questionBean.isOpen()) {
		%>
		<div class="clear_float" style="background: #F1F4FC;">
			<div style="float: right; padding: 6px 0;">
				<input type="button" value="补充问题"
					onclick="$Actions['reAddQuestionExtWindowAct']('questionId=<%=questionBean.getId()%>');">
				<input type="button" value="无满意答案"
					onclick="$IT.A('notGoodAnswerAct');">
			</div>
		</div>
		<%
			}
		%>
		<div id="questionExtListId"></div>
	</div>
	<div style="padding: 4px 0px;" class="inherit_c wrap_text">
		<div id="news_body">
			<%=questionBean.getContent()%>
		</div>
	</div>
</div>
<div class="idRemarks" id="idRemarks">
	<div id="question_remark"></div>
</div>
<style type="text/css">
.code_div {
	border-bottom: 1px solid #ccc;
	background: white;
	padding: 0 0 2px 0;
}

.code_span {
	padding: 0 0 2px 0;
	font-size: 10pt;
	background: #ccc;
	color: white;
	padding: 2px 10px;
}
</style>
<script type="text/javascript">
$ready(function() {
	$IT.bindJS(".__vote", false);
});
</script>