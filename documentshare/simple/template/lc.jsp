<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="net.simpleframework.web.WebUtils"%>
<%@ page import="net.simpleframework.util.StringUtils"%>
<html>
<jsp:include page="inc_head.jsp" flush="true"></jsp:include>
<body style="margin: 0px; padding: 0px;">
	<div align="center">
		<div id="t_main">
			<div id="t_header"><jsp:include page="header.jsp" flush="true"></jsp:include></div>
			<div id="t_body" class="clear_float">
				<table class="fixed_table" style="width: 100%; height: 100%;"
					cellspacing="0" cellpadding="0">
					<tr>
						<td width="22%" valign="top">
							<div class="left">
								<%
									final String left = WebUtils.putIncludeParameters(request,
											request.getParameter("left"));
									final String center = WebUtils.putIncludeParameters(request,
											request.getParameter("center"));
									if (StringUtils.hasText(left)) {
								%><jsp:include page="<%=left%>" flush="true"></jsp:include>
								<%
									}
								%>
							</div>
						</td>
						<td class="splitbar"></td>
						<td valign="top">
							<div class="center">
								<%
									if (StringUtils.hasText(center)) {
								%><jsp:include page="<%=center%>" flush="true"></jsp:include>
								<%
									}
								%>
							</div>
						</td>
					</tr>
				</table>
			</div>
			<jsp:include page="/simple/template/footer.jsp" flush="true"></jsp:include>
		</div>
	</div>
</body>
<script type="text/javascript">
	(function() {
		var sb = $$("#t_main .splitbar")[0];
		$Comp.createSplitbar(sb, sb.previous());
	})();
</script>
</html>



