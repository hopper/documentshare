<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="net.simpleframework.web.WebUtils"%>
<%@ page import="net.simpleframework.util.StringUtils"%>
<table width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<td align="center">
			<table width="100%" height="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td width="22%" valign="top" id="hidden_1">
						<div class="left">
							<%
								final String left = WebUtils.putIncludeParameters(request,
										request.getParameter("left"));
								final String center = WebUtils.putIncludeParameters(request,
										request.getParameter("center"));
								if (StringUtils.hasText(left)) {
							%><jsp:include page="<%=left%>" flush="true"></jsp:include>
							<%
								}
							%>
						</div>
					</td>
					<td class="splitbar" id="hidden_2"></td>
					<td valign="top">
						<div class="center">
							<%
								if (StringUtils.hasText(center)) {
							%><jsp:include page="<%=center%>" flush="true"></jsp:include>
							<%
								}
							%>
						</div>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<script type="text/javascript">
	(function() {
		var sb = $$("#t_main .splitbar")[0];
		$Comp.createSplitbar(sb, sb.previous());
	})();
</script>
<script type="text/javascript">
	function toggle_bk() {
		$('hidden_1').toggle();
		$('hidden_2').toggle();
	}
</script>



