var PagerUtil = {};
PagerUtil.rowId = function(o) {
	return (o = PagerUtil.row(o)) ? o.getAttribute("rowId") : null;
};
PagerUtil.attr = function(o,attr) {
	return (o = PagerUtil.row(o)) ? o.getAttribute(attr) : null;
};
PagerUtil.row = function(o) {
	return o.hasClassName("titem") ? o : $target(o).up(".titem");
};
PagerUtil.each = function(o) {
	var ele = $target(o).up(".titem");
	while (ele != null) {
		if (ele.hasClassName("titem")) {
			return ele;
		} else {
			ele = ele.parentNode;
		}
	}
	return o;
};
String.prototype.trim = function() {
	return this.replace(/(^\s*)|(\s*$)/g, "");
};
function getCookie(name, value) {
	var strCookie = document.cookie;
	var arrCookie = strCookie.split(";");
	for ( var i = 0; i < arrCookie.length; i++) {
		var arr = arrCookie[i].split("=");
		if (arr[0].trim() == name)
			return arr[1];
	}
	return value;
};
function addTextButton(id, action, td, isDate, value) {
	var ele = $Comp.textButton(id, function(ev) {
		if (isDate)
			$Actions[action].show();
		else
			$Actions[action]();
	});
	$Actions.readonly(ele.textObject);
	$(td).update(ele);
	if (arguments.length == 5) {
		$(id).value = value;
	}
}
$IT = {};
$IT.hidden = function(ss, id) {
	if ($$(ss).length == 0)
		$(id).style.display = 'none';
};
$IT.C = function(act) {
	$Actions[act].close();
};
$IT.R = function(act) {
	if($Actions[act])
		$Actions[act].refresh();
}
$IT.A = function(act, p) {
	if (arguments.length == 2) {
		$Actions[act](p);
	} else {
		$Actions[act]();
	}
}
$IT.bindEle = function(e, seletor, f) {
	$$(seletor).each(function(c) {
		c.observe(e, function() {
			f(c);
		});
	});
};
$IT.bindJS = function(seletor, flag) {
	$$(seletor).each(function(c) {
		var act = "";
		if (flag) {
			act = c.id;
		} else {
			act = c.className;
			act = act.split(" ")[0];
		}
		act = act.substring(2, act.length);
		var param = c.getAttribute("param");
		var p = "";
		if (param != null && param != '' && param != 'undefined') {
			p = param;
		}
		c.observe("click", function() {
			$Actions[act + "Act"](p);
		});
	});
};