<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page
	import="net.simpleframework.organization.account.AccountSession"%>
<%@ page
	import="net.simpleframework.organization.component.login.LoginUtils"%>
<%@ page import="net.simpleframework.organization.account.IAccount"%>
<%@ page import="net.simpleframework.organization.OrgUtils"%>
<%@ page import="net.simpleframework.util.HTMLBuilder"%>
<%@ page import="net.simpleframework.applets.invite.InviteUtils"%>
<%@ page import="net.simpleframework.web.page.PageRequestResponse"%>
<%@ page import="net.simpleframework.my.space.MySpaceUtils"%>
<%
	final PageRequestResponse requestResponse = new PageRequestResponse(request, response);
%>
<%
	IAccount account = AccountSession.getLogin(session);
	if (account != null) {
%>
<a href="/space.html?t=document">管理中心</a>
<img style="cursor: pointer;" src="/simple/template/images/down.gif" alt="移动 鼠标到这里"
	class="my_navTooltip_class">
<%=HTMLBuilder.SEP%>
你好,
<a
	onclick="$Actions['editUserWindow']('<%=OrgUtils.um().getUserIdParameterName()%>=<%=account.getId()%>');"><%=account.user()%></a>
<span> ( </span>
<a onclick="$Actions['ajaxLogout']();">注销</a>
<span> )</span>
<%
	} else {
%><a
	onclick="$Actions.loc('<%=LoginUtils.getLocationPath()%>');">登录</a><%=HTMLBuilder.SEP%><a
	href="/regist.html">注册</a>
<%
	}
%>