<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="net.simpleframework.content.blog.BlogUtils"%>
<%
	final String my_blog_c = BlogUtils.deployPath + "jsp/my_blog_c.jsp";
	final String my_blog_lr = BlogUtils.deployPath
			+ "jsp/my_blog_lr.jsp";
%>
<div class="c_left">
	<jsp:include page="<%=my_blog_c%>"></jsp:include>
</div>
<div class="c_right">
	<jsp:include page="<%=my_blog_lr%>"></jsp:include>
</div>