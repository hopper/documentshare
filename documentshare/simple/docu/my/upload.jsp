<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><%@page
	import="net.documentshare.docu.EDocuFunction"%><%@page
	import="net.documentshare.docu.DocuUtils"%>

<div>
	<div class="simple_toolbar1">
		<ol>
			<li>
				可以上传单个大小为500MB的文档文件。
			</li>
			<li>
				支持的文档格式为doc, docx, ppt, pptx, xls, xlsx, pdf, xml, txt
			</li>
			<li>
				支持的视频格式为mp4, flv
			</li>
			<li>
				不准上传色情或违法反动的资源
			</li>
			<li>
				不准上传侵犯他人的版权的资源
			</li>
		</ol>
	</div>
	<div id=docu_upload></div>
</div>
<style type="text/css">
#docu_content_ .l {
	width: 100px;
	text-align: right;
}

#docu_content_ {
	background: #f7f7ff;
	padding: 6px 8px;
}
</style>
