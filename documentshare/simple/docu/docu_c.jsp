<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><%@page
	import="net.documentshare.docu.EDocuType1"%><%@page
	import="net.simpleframework.util.ConvertUtils"%><%@page
	import="net.documentshare.utils.ItSiteUtil"%><%@page
	import="net.documentshare.docu.DocuUtils"%><%@page
	import="net.simpleframework.web.page.PageRequestResponse"%><%@page import="net.simpleframework.util.StringUtils"%>



<%
	final PageRequestResponse requestResponse = new PageRequestResponse(request, response);
	int t = ConvertUtils.toInt(request.getParameter("t"), 0);
	String c = request.getParameter("c");
	String _docu_topic = request.getParameter("_docu_topic");
	String tagId = request.getParameter("tagId");
	final int catalogId = ConvertUtils.toInt(request.getParameter("catalogId"), 0);
	boolean isNode = DocuUtils.isNode(catalogId);
	if (t != 0 && !ItSiteUtil.isManage(requestResponse, DocuUtils.applicationModule)) {
		t = 0;
	}
%>
<jsp:include page="docu_bar.jsp"></jsp:include>
<div class="simple_toolbar1">
<%
	if(t == EDocuType1.docu.ordinal()) {
	%>
	<jsp:include page="/simple/docu/all/alldocu.jsp"></jsp:include>
	<%
		} else if(!StringUtils.hasText(tagId)&&!StringUtils.hasText(_docu_topic)&&!StringUtils.hasText(c)&&!isNode){
	%>
	<jsp:include page="docu_list_nonnode.jsp"></jsp:include>
	<%
		} else {
	%>
	<jsp:include page="docu_list.jsp"></jsp:include>
	<%
		}
	%>
</div>
