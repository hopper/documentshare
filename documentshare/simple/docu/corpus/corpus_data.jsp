<%@page import="net.simpleframework.content.EContentType"%>
<%@page import="net.documentshare.docu.DocuBean"%>
<%@page import="net.documentshare.docu.DocuUtils"%>
<%@page import="java.util.Map"%>
<%@page import="net.documentshare.docu.corpus.CorpusUtils"%>
<%@page import="net.simpleframework.web.page.PageRequestResponse"%>
<%@page import="net.documentshare.utils.ItSiteUtil"%>
<%@page import="net.simpleframework.util.StringUtils"%>
<%@page import="net.documentshare.docu.corpus.CorpusBean"%>
<%@page
	import="net.simpleframework.web.page.component.ui.pager.PagerUtils"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><%@page
	import="net.simpleframework.util.ConvertUtils"%>


<%
	final PageRequestResponse requestResponse = new PageRequestResponse(request, response);
	final int t = ConvertUtils.toInt(request.getParameter("t"), 0);
	final int o1 = ConvertUtils.toInt(request.getParameter("o"), 0);
	final List<?> data = PagerUtils.getPagerList(request);
	if (data == null || data.size() == 0) {
		return;
	}
	for (Object o : data) {
		final CorpusBean corpusBean = (CorpusBean) o;
		List<Map<String, String>> dataDoc = CorpusUtils.queryBycorpusId(corpusBean, 4);
		String fontImage = CorpusUtils.getCorpusFrontImage(corpusBean, requestResponse);
%>
<div class="titem">
	<input type="hidden" name="corpusId" class="corpusId"
		value="<%=corpusBean.getId().getValue()%>">
	<table class="album_doc" width="100%">
		<tr>
			<td>
				<table>
					<tr>
						<td class="albumImg">
							<a target="_blank"
								href="<%=CorpusUtils.getMyCorposDocViewUrl(corpusBean.getId().getValue().toString())%>?userId=<%=corpusBean.getUserId()%>"><img
									border="0" alt="<%=corpusBean.getName()%>" class="preimg120"
									src="<%=fontImage%>"> </a>
						</td>
						<td class="albumInfo" valign="top">
							<div class="name">
								<%=CorpusUtils.wrapOpenLink(corpusBean)%>
							</div>
							<div class="intro">
								<%=StringUtils.substring(corpusBean.getDescription(), 20)%>
							</div>
							<div class="user">
								会员：
								<a target="_blank"><%=ItSiteUtil.getUserById(corpusBean.getUserId()).getText()%></a>
								<%
									if (corpusBean.getContentType() == EContentType.recommended) {
								%>
								<img style="margin-left: 5px;"
									src="<%=request.getContextPath()%>/simple/common/images/recommended.gif">
								<%
									}
								%>
							</div>
							<div class="intro">
								阅读：
								<span class="nnum"><%=corpusBean.getReadTime()%></span>
							</div>
							<%
								if (dataDoc != null) {
							%>
							<ul>
								<%
									for (Map<String, String> d : dataDoc) {
								%>
								<li>
									<a target="_blank"
										href="<%=DocuUtils.applicationModule.getViewUrl(d.get("docId"))%>"><%=d.get("fileName")%>
									</a>
									<%=d.get("pageCount")%>页
								</li>
								<%
									}
								%>
							</ul>
						</td>
					</tr>
				</table>
			</td>
			<%
				if (o1 == 1 && ItSiteUtil.isManage(requestResponse, DocuUtils.applicationModule)) {
			%>
			<td style="width: 22px;">
				<a href="javascript:void(0);" class="corpus down_menu_image"></a>
			</td>
			<%
				}
			%>
		</tr>
	</table>
</div>
<%
	}
	}
%>
