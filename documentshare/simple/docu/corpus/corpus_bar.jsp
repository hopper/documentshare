<%@page import="net.documentshare.docu.corpus.CorpusUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="net.simpleframework.web.page.PageRequestResponse"%>
<%@ page import="net.simpleframework.util.StringUtils"%>
<%@ page import="net.simpleframework.web.WebUtils"%>
<%@page import="net.simpleframework.web.IWebApplicationModule"%>
<%@page import="net.simpleframework.util.ConvertUtils"%><%@page
	import="net.documentshare.utils.ItSiteUtil"%><%@page
	import="net.documentshare.docu.DocuUtils"%><%@page
	import="net.documentshare.docu.EDocuFunction"%>

<%
	final PageRequestResponse requestResponse = new PageRequestResponse(request, response);
	final String appUrl = "docu_corpus.html";
	final int t = ConvertUtils.toInt(request.getParameter("t"), 0);
	final int o = ConvertUtils.toInt(request.getParameter("o"), 0);
	final boolean manager = ItSiteUtil.isManage(requestResponse, DocuUtils.applicationModule);
	final long count = CorpusUtils.getCount();
%>
<div id="__barHeader"></div>
<div class="simple_toolbar2">
	<table width="100%" cellpadding="1" cellspacing="0">
		<tr>
			<td width="100%">
				<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td id="__blog_sech" align="right"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td width="100%">
				<div style="display: none;" id="sech_pane_params"
					class="sech_pane_params">
					<table cellpadding="2" cellspacing="0" style="width: 420px;">
						<tr>
							<td class="l">
								标题
							</td>
							<td class="v">
								<input type="text" name="corpusTile" id="corpusTile" />
							</td>
						</tr>
						<tr>
							<td class="l">
								分类
							</td>
							<td class="v">
								<a id="__catalogSelectDict">添加分类</a>
								<div id="_corpus_catalog"></div>
							</td>
						</tr>
						<tr>
							<td class="l"></td>
							<td class="v">
								<input type="button" class="button2" value="#(Button.Ok)"
									onclick="M_UTILS.search_submit(this);" />
								<input type="button" value="#(Button.Cancel)"
									onclick="this.up('div').$toggle();" />
							</td>
						</tr>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td width="100%"><%=CorpusUtils.tabs(requestResponse)%></td>
		</tr>
		<%
			if (o != 1) {
		%>
		<tr>
			<td align="right">
				<a class="<%=(t == 0 ? "a2 nav_arrow" : "")%>"
					href="/docu_corpus/<%=o %>-0.html">最新</a><span
					style="margin: 0px 4px;">|</span><a
					class="<%=(t == 1 ? "a2 nav_arrow" : "")%>"
					href="/docu_corpus/<%=o %>-1.html">最多阅读</a><span
					style="margin: 0px 4px;">|</span><a
					class="<%=(t == 2 ? "a2 nav_arrow" : "")%>"
					href="/docu_corpus/<%=o %>-2.html">推荐文辑</a><span
					style="margin: 0px 4px;">|</span><a
					class="<%=(t == 3 ? "a2 nav_arrow" : "")%>"
					href="/docu_corpus/<%=o %>-3.html">最多收藏</a>
			</td>
		</tr>
		<%
			}
		%>
	</table>
</div>
<script type="text/javascript">

var M_UTILS = {
	search_submit : function(obj) {
		var sp = obj.up('.sech_pane_params');
		var catalog = "_corpus_catalog=";
		var i = 0;
		$$("#_corpus_catalog div[id]").each(function(d) {
			if (i++ > 0)
				catalog += ";";
			catalog += d.id;
		});
		var params = $$Form(sp).addParameter(catalog);
		$Actions['corpusListPaperAct_docu'](params);
	},
	insert_forum : function(selects) {
		if (!selects.any(function(node) {
			return node.selected;
		})) {
			return;
		}
		var c = $("_corpus_catalog");
		selects.each(function(node) {
			if (!node.selected || c.down("#" + node.id)) {
				return;
			}
			c.insert(new Element("div", {
				id : node.id
			}).insert(new Element("span").update(node.text)).insert(
					new Element("a", {
						className : "delete_image",
						onclick : "this.up().remove();"
					})));
		});
		return true;
	}
};
</script>
<script type="text/javascript">
(function() {
		var sb = $Comp.searchButton(function(sp) {
			$Actions.loc("<%=appUrl%>".addParameter("c=" + $F(sp.down(".txt"))));
		}, function(sp) {
			$('sech_pane_params').$toggle();
		}, '当前已有<%=count%>份文档', 210);
		$("__blog_sech").update(sb);
		<%final String c = WebUtils.toLocaleString(request.getParameter("c"));
			if (StringUtils.hasText(c)) {%>
		sb.down(".txt").setValue("<%=c%>");
<%}%>
	})();
	$IT.bindJS("#__catalogSelectDict", true);
</script>