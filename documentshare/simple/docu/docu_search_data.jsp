<%@page import="net.simpleframework.web.WebUtils"%>
<%@page import="net.simpleframework.web.page.PageRequestResponse"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page
	import="net.simpleframework.web.page.component.ui.pager.PagerUtils"%>
<%@ page import="java.util.List"%><%@page
	import="net.simpleframework.util.HTMLBuilder"%><%@page
	import="net.documentshare.docu.DocuBean"%><%@page
	import="net.documentshare.utils.ItSiteUtil"%><%@page
	import="net.documentshare.docu.DocuUtils"%><%@page
	import="net.simpleframework.content.ContentUtils"%><%@page
	import="net.simpleframework.util.StringUtils"%><%@page
	import="net.simpleframework.util.DateUtils"%><%@page
	import="net.documentshare.docu.EDocuFunction"%><%@page
	import="net.simpleframework.util.ConvertUtils"%><%@page
	import="net.documentshare.impl.AbstractCatalog"%><%@page
	import="net.documentshare.docu.DocuCatalog"%><%@page
	import="net.simpleframework.organization.IUser"%><%@page
	import="net.simpleframework.util.IoUtils"%>
<%
	final String _search_topic = WebUtils.toLocaleString(request.getParameter("_search_topic"));
	final PageRequestResponse requestResponse = new PageRequestResponse(request, response);
%>
<style>
#FilterForm {
	margin-bottom: 4px;
}

#FilterForm a {
	margin: 0 2px;
	font-weight: bold;
	vertical-align: middle;
}

#FilterForm span {
	margin: 0 2px;
	font-weight: bold;
	vertical-align: middle;
}

.pager .pager_top_block .pager_title {
	margin-left: 3px;
}

.pager .pager_head {
	margin-right: 6px;
}

.listpager {
	
}

.listpager .titem {
	display: inline-block;
	vertical-align: top;
	border: 1px solid #EEE;
	margin-top: -1px;
	width: 99%;
}

.iconpager .titem {
	display: inline-block;
	vertical-align: top;
	border: 1px solid #EEE;
	margin-top: -1px;
	width: 49%;
}

.listpager .titem:HOVER,.iconpager .titem:HOVER {
	background-color: #F8F8F8;
}

.listpager .title,.iconpager .title {
	font-weight: bold;
}

.split_docu {
	padding: 0px 5px;
}
</style>
<div class="listpager" id="_tablepaper">
	<div id="FilterForm">
		<div>
			<span style="padding-right: 5px;">属性过滤</span><%=DocuUtils.searchFilterDocu(requestResponse)%>
		</div>
		<div>
			<span style="padding-right: 5px;">条件排序</span><%=DocuUtils.searchSortDocu(requestResponse)%>
		</div>
	</div>
	<%
		final List<?> data = PagerUtils.getPagerList(request);
		if (data == null && data.size() == 0) {
			return;
		}
		for (Object o : data) {
			final DocuBean docuBean = (DocuBean) o;
	%>
	<div class="titem" style="padding-left: 0px;">
		<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td width="20" class="f4">
					<img alt="<%=docuBean.getExtension()%>"
						src="<%=DocuUtils.getFileImage(requestResponse, docuBean)%>">
					<a
						href="<%=DocuUtils.applicationModule.getViewUrl(docuBean.getId())%>"
						target="_blank"><%=ItSiteUtil.markContent(_search_topic,docuBean.getTitle())%></a>
				</td>
			</tr>
			<tr>
				<td>
					<p><%=ItSiteUtil.markContent(_search_topic,ItSiteUtil.getShortString(docuBean.getContent(), 300, false))%></p>
				</td>
			</tr>
			<tr>
				<td>
					<table>
						<tr>
							<td>
								时间：<%=ConvertUtils.toDateString(docuBean.getCreateDate())%>
								分类：<%=AbstractCatalog.Utils.buildCatalog(docuBean.getCatalogId(), DocuCatalog.class, DocuUtils.applicationModule, false)%>
							</td>
						</tr>
						<tr>
							<td>
								评分：
								<span class="nnum docuStar" id="docuStar"><%=docuBean.getTotalGrade()%></span><span
									class="split_docu">|</span>下载：
								<span class="nnum"><%=docuBean.getDownCounter()%></span><span
									class="split_docu">|</span>大小：
								<span class="nnum"><%=IoUtils.toFileSize(docuBean.getFileSize())%></span><span
									class="split_docu">|</span>阅读：
								<span class="nnum"><%=docuBean.getViews()%></span><span
									class="split_docu">|</span>积分：
								<span class="nnum"><%=docuBean.getPoint()%></span><span
									class="split_docu">|</span>上传者：
								<span><%=ContentUtils.getAccountAware().wrapAccountHref(requestResponse, docuBean.getUserId(), docuBean.getUserText())%></span>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
	<%
		}
	%>
</div>
