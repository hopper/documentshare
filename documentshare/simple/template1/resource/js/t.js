$ready(function() {
	var header = $('t_header');
	var footer = $('t_footer');
	var m = $('t_main');
	var l = m.down('.left');
	var c = m.down('.center');
	var r = function() {
		var height = document.viewport.getHeight() - 
			header.getHeight() - footer.getHeight();
		var style = 'height: ' + height + 'px';
		m.setStyle(style);
		if (l)
			l.setStyle(style);
		if (c)
			c.setStyle(style);
	};
	r.call();
	Event.observe(window, "resize", r);
});