<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="net.simpleframework.my.space.MySpaceUtils"%>
<%
	final String my_space_left = MySpaceUtils.deployPath
			+ "jsp/my_space_left.jsp";
	final String my_space_right = MySpaceUtils.deployPath
			+ "jsp/my_space_right.jsp";
%>
<div style="float: left; width: 270px; padding: 0 8px 0 2px;">
	<jsp:include page="<%=my_space_left%>"></jsp:include>
</div>
<div style="float: right; width: 705px;">
	<jsp:include page="<%=my_space_right%>"></jsp:include>
</div>