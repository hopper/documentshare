<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><%@page
	import="net.simpleframework.applets.openid.OpenIDUtils"%>

<%
	final String openid = OpenIDUtils.deployPath + "jsp/openid_list.jsp";
%>
<%@ page
	import="net.simpleframework.organization.component.login.LoginUtils"%>
<table width="100%" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td width="55%" align="center" style="padding: 60px 20px;">
			<div style="text-align: center; padding-bottom: 10px;" class="f2">
				欢迎您再次登入“嵌入式工程师文件分享社区”
			</div>
			<div style="padding: 30px 20px; width: 320px;"
				class="simple_toolbar3">
				<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td align="center"
							style="padding: 20px 10px 40px 0px; border-right: 3px double #ccc;">
							<img src="<%=LoginUtils.getHomePath() + "/images/login.png"%>" />
						</td>
						<td style="padding-left: 10px;" id="__default_login"></td>
					</tr>
				</table>
				<div class="f3"
					style="border-top: 3px double #ccc; padding: 10px 0px;">
					同时还支持以下几种登录方法（请点击以下图标进入登录页面）：
				</div>
				<jsp:include page="<%=openid%>"></jsp:include>
		</div>
		</td>
	</tr>
</table>
