package net.simpleframework.workflow.schema;

import net.simpleframework.util.GenId;
import net.simpleframework.util.StringUtils;

import org.dom4j.Element;

/**
 * 这是一个开源的软件，请在LGPLv3下合法使用、修改或重新发布。
 * 
 * @author 陈侃(cknet@126.com, 13910090885)
 *         http://code.google.com/p/simpleframework/
 *         http://www.simpleframework.net
 */
public abstract class Node extends AbstractNode {
	private String id, text;

	private String description;

	public Node(final Element dom4jElement, final ProcessNode parent) {
		super(dom4jElement, parent);
	}

	public String getId() {
		if (!StringUtils.hasText(id)) {
			id = GenId.genUUID();
		}
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(final String text) {
		this.text = text;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return getText();
	}

	@Override
	protected String[] elementAttributes() {
		return new String[] { "description" };
	}

	static Element addNode(final ProcessNode processNode, final String name) {
		return addElement(processNode, "nodes", name);
	}
}
