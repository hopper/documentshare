package net.simpleframework.content.bbs;

import net.simpleframework.ado.DataObjectManagerFactory;
import net.simpleframework.content.AbstractMgrToolsAction;
import net.simpleframework.content.IContentApplicationModule;

/**
 * 这是一个开源的软件，请在LGPLv3下合法使用、修改或重新发布。
 * 
 * @author 陈侃(cknet@126.com, 13910090885)
 *         http://code.google.com/p/simpleframework/
 *         http://www.simpleframework.net
 */
public class BbsAction extends AbstractMgrToolsAction {

	@Override
	protected void doStatRebuild() {
		BbsUtils.doStatRebuild();

		// test cache
		DataObjectManagerFactory.resetAll();
		System.gc();
	}

	@Override
	public IContentApplicationModule getApplicationModule() {
		return BbsUtils.applicationModule;
	}
}
