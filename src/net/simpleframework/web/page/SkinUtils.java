package net.simpleframework.web.page;

import javax.servlet.http.HttpSession;

import net.simpleframework.util.StringUtils;
import net.simpleframework.web.page.component.AbstractComponentRegistry;

/**
 * 这是一个开源的软件，请在LGPLv3下合法使用、修改或重新发布。
 * 
 * @author 陈侃(cknet@126.com, 13910090885)
 *         http://code.google.com/p/simpleframework/
 *         http://www.simpleframework.net
 */
public abstract class SkinUtils {
	public final static String DEFAULT_SKIN = "blue";

	private final static String SESSION_NAME_SKIN = "__skin";

	public static String getSkin(final PageRequestResponse requestResponse, final String userSkin) {
		final String sessionSkin = (String) requestResponse.getSessionAttribute(SESSION_NAME_SKIN);
		return StringUtils.text(sessionSkin, userSkin, DEFAULT_SKIN);
	}

	public static void setSessionSkin(final HttpSession httpSession, final String skin) {
		httpSession.setAttribute(SkinUtils.SESSION_NAME_SKIN, skin);
	}

	public static void setComponentSkin(final String registryName, final String skin) {
		final AbstractComponentRegistry registry = AbstractComponentRegistry
				.getRegistry(registryName);
		if (registry != null) {
			registry.getComponentResourceProvider().setSkin(skin);
		}
	}
}
