package net.simpleframework.swing;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.table.TableCellRenderer;

import net.simpleframework.util.StringUtils;

import org.jdesktop.swingx.renderer.CellContext;
import org.jdesktop.swingx.renderer.ComponentProvider;
import org.jdesktop.swingx.renderer.DefaultTableRenderer;
import org.jdesktop.swingx.renderer.JRendererLabel;
import org.jdesktop.swingx.renderer.StringValue;

/**
 * 这是一个开源的软件，请在LGPLv3下合法使用、修改或重新发布。
 * 
 * @author 陈侃(cknet@126.com, 13910090885)
 *         http://code.google.com/p/simpleframework/
 *         http://www.simpleframework.net
 */
public abstract class JTableExUtils {
	static StringValue defaultStringValue = new StringValue() {
		private static final long serialVersionUID = -6682796733955379757L;

		@Override
		public String getString(final Object value) {
			return value != null ? value.toString() : "";
		}
	};

	final static JRendererLabel lineNoHeaderRenderer = new JRendererLabel();

	static {
		lineNoHeaderRenderer.setBorder(UIManager.getBorder("TableHeader.cellBorder"));
	}

	static Color lineNoColor = Color.decode("#018006");

	static Font lineNoFont = new Font(Font.SANS_SERIF, Font.PLAIN, 10);

	static Font lineNoFont2 = new Font(Font.SANS_SERIF, Font.PLAIN, 11);

	static Font lineNoFont3 = new Font(Font.SANS_SERIF, Font.PLAIN, 12);

	static Border tableHeaderCellBorder = BorderFactory
			.createCompoundBorder(UIManager.getBorder("TableHeader.cellBorder"),
					BorderFactory.createEmptyBorder(0, 0, 0, 2));

	final static TableCellRenderer lineNoCellRenderer = new TableCellRenderer() {
		final JRendererLabel lbl = new JRendererLabel();
		{
			lbl.setHorizontalAlignment(SwingConstants.CENTER);
			lbl.setBorder(tableHeaderCellBorder);
			lbl.setForeground(lineNoColor);
		}

		@Override
		public Component getTableCellRendererComponent(final JTable table, final Object value,
				final boolean isSelected, final boolean hasFocus, final int row, final int column) {
			if (row < 99) {
				lbl.setFont(lineNoFont3);
			} else if (row < 999) {
				lbl.setFont(lineNoFont2);
			} else {
				lbl.setFont(lineNoFont);
			}
			lbl.setText(String.valueOf(row + 1));
			return lbl;
		}
	};

	final static TableCellRenderer multipleLineRenderer = new DefaultTableRenderer(
			new ComponentProvider<JComponent>() {
				private static final long serialVersionUID = -6573362078819481127L;

				@Override
				protected void configureState(final CellContext context) {
					final JTableEx table = (JTableEx) context.getComponent();
					final JTableExColumn column = table.getPrivateTableExColumn(context.getColumn());

					final MultipleLineLabel mlLabel = (MultipleLineLabel) rendererComponent;
					mlLabel.setText(table.convertToString(column, context.getValue()));
				}

				@Override
				protected JComponent createRendererComponent() {
					final MultipleLineLabel mlLabel = new MultipleLineLabel();
					return mlLabel;
				}

				@Override
				protected void format(final CellContext context) {
				}
			});

	final static TableCellRenderer percentRenderer = new DefaultTableRenderer(
			new ComponentProvider<JComponent>() {
				private static final long serialVersionUID = -6573362078819481127L;

				@Override
				protected void configureState(final CellContext context) {
					final JTableEx table = (JTableEx) context.getComponent();
					final JPanel jpanel = (JPanel) rendererComponent;
					final JProgressBar bar = (JProgressBar) jpanel.getComponent(0);
					if (table.isFixTable()) {
						jpanel.setBorder(tableHeaderCellBorder);
						if (!context.isSelected()) {
							jpanel.setBackground(lineNoHeaderRenderer.getBackground());
							bar.setBackground(lineNoHeaderRenderer.getBackground());
						}
					} else {
						if (!context.isSelected()) {
							jpanel.setBackground(table.getBackground());
							bar.setBackground(table.getBackground());
						}
					}

					final Object value = context.getValue();
					if (value instanceof Number) {
						bar.setValue((int) (((Number) value).doubleValue() * 100.0d));
						final JTableExColumn column = table.getPrivateTableExColumn(context.getColumn());
						if (!StringUtils.hasText(column.getFormat())) {
							column.setFormat("#0.##%");
						}
						bar.setString(table.convertToString(column, value));
					}
				}

				@Override
				protected void format(final CellContext context) {
				}

				@Override
				protected JComponent createRendererComponent() {
					final JPanel jpanel = new JPanel(new BorderLayout());
					final JProgressBar bar = new JProgressBar(0, 100);
					jpanel.add(bar);
					bar.setBorderPainted(false);
					bar.setStringPainted(true);
					return jpanel;
				}
			});

	static String colorRed(final Object o) {
		return "<font color=\"red\">" + o + "</font>";
	}
}
