package net.documentshare.mytag;

import java.util.ArrayList;
import java.util.List;

import net.simpleframework.ado.DataObjectManagerUtils;
import net.simpleframework.ado.db.ExpressionValue;
import net.simpleframework.ado.db.ITableEntityManager;
import net.simpleframework.ado.db.SQLValue;
import net.simpleframework.content.IContentPagerHandle;
import net.simpleframework.core.ado.IDataObjectQuery;
import net.simpleframework.core.id.ID;
import net.simpleframework.util.ConvertUtils;
import net.simpleframework.util.StringUtils;
import net.simpleframework.util.db.DbUtils;
import net.simpleframework.web.WebUtils;
import net.simpleframework.web.page.PageRequestResponse;

public abstract class MyTagUtils {
	public static IMyTagApplicationModule applicationModule;
	public static String deploy = "/simple/mytag";
	public static String deployPath;

	public static ETagType getVtype(final PageRequestResponse requestResponse) {
		return ConvertUtils.toEnum(ETagType.class, requestResponse.getRequestParameter(IContentPagerHandle._VTYPE));
	}

	public static ITableEntityManager getTableEntityManager(final Class<?> beanClazz) {
		return DataObjectManagerUtils.getTableEntityManager(applicationModule, beanClazz);
	}

	public static ITableEntityManager getTableEntityManager() {
		return DataObjectManagerUtils.getTableEntityManager(applicationModule);
	}

	public static void syncTags(final ETagType vtype, final ID catalogId, String keywords, final ID rid) {
		if (vtype == null) {
			return;
		}
		if (!StringUtils.hasText(keywords)) {
			return;
		}
		final ITableEntityManager tag_mgr = getTableEntityManager(MyTagBean.class);
		final ITableEntityManager tagr_mgr = getTableEntityManager(MyTagRBean.class);
		keywords = StringUtils.replace(keywords, ",", " ");
		keywords = StringUtils.replace(keywords, ";", " ");

		final ArrayList<MyTagBean> inserts = new ArrayList<MyTagBean>();
		final ArrayList<MyTagBean> updates = new ArrayList<MyTagBean>();
		for (String tag : StringUtils.split(keywords, " ")) {
			tag = tag.trim().toLowerCase();
			MyTagBean tagBean = tag_mgr.queryForObject(new ExpressionValue("vtype=? and catalogid=? and tagtext=?", new Object[] { vtype, catalogId,
					tag }), MyTagBean.class);
			if (tagBean == null) {
				tagBean = new MyTagBean();
				tagBean.setVtype(vtype);
				tagBean.setFrequency(1);
				tagBean.setCatalogId(catalogId);
				tagBean.setTagText(tag);
				inserts.add(tagBean);
			} else {
				tagBean.setFrequency(tagBean.getFrequency() + 1);
				tag_mgr.update(tagBean);
				updates.add(tagBean);
			}
		}
		tag_mgr.insert(inserts.toArray());
		tag_mgr.update(updates.toArray());

		inserts.addAll(updates);
		final ArrayList<MyTagRBean> inserts2 = new ArrayList<MyTagRBean>();
		tagr_mgr.delete(new ExpressionValue("rid=?", new Object[] { rid }));
		for (final MyTagBean tagBean : inserts) {
			final MyTagRBean tagRBean = new MyTagRBean();
			tagRBean.setTagId(tagBean.getId());
			tagRBean.setRid(rid);
			inserts2.add(tagRBean);
		}
		tagr_mgr.insert(inserts2.toArray());
	}

	public static void deleteRTags(final Object[] rid) {
		getTableEntityManager(MyTagRBean.class).delete(new ExpressionValue(DbUtils.getIdsSQLParam("rid", rid.length), rid));
	}

	public static MyTagBean getTagBeanById(final Object id) {
		return getTableEntityManager(MyTagBean.class).queryForObjectById(id, MyTagBean.class);
	}

	public static IDataObjectQuery<MyTagBean> layoutTags(final PageRequestResponse requestResponse) {
		final ETagType vtype = getVtype(requestResponse);
		final ID catalogId = applicationModule.getCatalogId(requestResponse);
		final StringBuffer sql = new StringBuffer();
		final List<Object> ol = new ArrayList<Object>();
		sql.append("vtype=?");
		ol.add(vtype);
		if (catalogId != null && !catalogId.equals2(0)) {
			sql.append(" and catalogid=?");
			ol.add(catalogId);
		}
		final IDataObjectQuery<MyTagBean> qs = getTableEntityManager(MyTagBean.class).query(
				new ExpressionValue(sql.toString() + " order by ttype desc, views desc", ol.toArray()), MyTagBean.class);
		if (qs != null) {
			final int rows = ConvertUtils.toInt(requestResponse.getRequestParameter("rows"), 6);
			if (rows > 0) {
				qs.setCount(rows);
			}
		}
		return qs;
	}

	public static void doTagsRebuild(final ETagType vtype, final ID catalogId) {
		final ITableEntityManager tag_mgr = getTableEntityManager(MyTagBean.class);
		final ITableEntityManager tagr_mgr = getTableEntityManager(MyTagRBean.class);

		final String tag_name = tag_mgr.getTablename();
		final StringBuilder sql = new StringBuilder();
		final Object[] params = new Object[] { vtype, catalogId };
		sql.append("update ").append(tag_name);
		sql.append(" a set frequency=(select count(tagid) from ");
		sql.append(tagr_mgr.getTablename()).append(" where tagid=a.id) where a.vtype=? and a.catalogid=?");
		tag_mgr.execute(new SQLValue(sql.toString(), params));

		sql.setLength(0);
		sql.append("delete from ").append(tag_name).append(" where frequency=0 and vtype=? and catalogid=?");
		tag_mgr.execute(new SQLValue(sql.toString(), params));
		tag_mgr.reset();
	}

	public static void updateViews(final PageRequestResponse requestResponse, final MyTagBean tagBean) {
		WebUtils.updateViews(requestResponse.getSession(), getTableEntityManager(MyTagBean.class), tagBean);
	}

	public static final String[] fontWeight = new String[] { "arial", "comic sans ms", "lucida sans unicode", "georgia", "trebuchet ms",
			"courier new", "times new roman" };

	public static String xmlTagsLayout() {
		return deploy + "/tags_layout.xml";
	}

}
