package net.documentshare;

import java.io.IOException;

import javax.servlet.ServletContext;

import net.simpleframework.applets.notification.component.window.MessageWindowRegistry;
import net.simpleframework.web.page.PageConfig;
import net.simpleframework.web.page.PageContext;
import net.simpleframework.web.page.component.ComponentRegistryFactory;

public class DSharePageContext extends PageContext {
	@Override
	protected PageConfig createPageConfig() {
		return new PageConfig(this) {

			@Override
			public boolean isGzipResponse() {
				return true;
			}
		};
	}
	@Override
	public void doInit(ServletContext servletContext) throws IOException {
		super.doInit(servletContext);
	}
}
