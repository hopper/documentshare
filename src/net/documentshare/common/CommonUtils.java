/**
 * 
 */
package net.documentshare.common;

public class CommonUtils {
	public static String deploy = "";
	public static String deployPath;
	public static ICommonApplicationModule applicationModule;

	public static String getContent(final ECommonType type) {
		final CommonBean commonBean = applicationModule.getCommonBean(type);
		if(commonBean==null){
			return "";
		}
		return commonBean.getContent();
	}
	
	public static CommonBean setContent(final ECommonType type,final String content){
		CommonBean commonBean = CommonUtils.applicationModule.getCommonBean(type);
		if (commonBean == null) {
			commonBean = new CommonBean();
			commonBean.setType(type);
		}
		commonBean.setContent(content);
		CommonUtils.applicationModule.doUpdate(commonBean);
		return commonBean;
	}
}
