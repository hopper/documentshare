package net.documentshare.question;

import java.util.Date;

import net.documentshare.impl.AbstractCommonBeanAware;

public class QuestionBean extends AbstractCommonBeanAware {
	private String title;// 标题
	private String keywords;//关键字
	private Date limitTime;//限时时间
	private int reward;// 酬金，多少积分
	private boolean email = false;
	private EQuestionStatus status;//关闭状态

	/**
	 * 是否开启
	 * @return
	 */
	public boolean isOpen() {
		return status == EQuestionStatus.noraml && limitTime.getTime() > System.currentTimeMillis();
	}

	public void setStatus(EQuestionStatus status) {
		this.status = status;
	}

	public boolean checkOpen() {
		if (status != EQuestionStatus.noraml) {
			return false;
		}
		if (limitTime.getTime() > System.currentTimeMillis()) {
			return false;
		}
		setStatus(EQuestionStatus.time);
		return true;
	}

	public EQuestionStatus getStatus() {
		return status == null ? EQuestionStatus.noraml : status;
	}

	public void setEmail(boolean email) {
		this.email = email;
	}

	public boolean isEmail() {
		return email;
	}

	public String getTitle() {
		return title;
	}

	@Override
	public String toString() {
		return getTitle();
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setLimitTime(Date limitTime) {
		this.limitTime = limitTime;
	}

	public Date getLimitTime() {
		return limitTime;
	}

	public void setReward(int reward) {
		this.reward = reward;
	}

	public int getReward() {
		return reward;
	}
}
