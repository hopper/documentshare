package net.documentshare.question;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import net.documentshare.utils.DateUtils;
import net.documentshare.utils.ItSiteUtil;
import net.simpleframework.ado.db.ITableEntityManager;
import net.simpleframework.ado.db.SQLValue;
import net.simpleframework.core.ado.IDataObjectQuery;
import net.simpleframework.web.page.PageRequestResponse;

public final class QuestionUtils {
	public static String questionId = "questionId";
	public static String deploy = "/simple/question";
	public static String deployPath;
	public static IQuestionApplicationModule applicationModule;

	public static QuestionBean getQuestionBean(final PageRequestResponse requestResponse) {
		return applicationModule.getBean(QuestionBean.class, requestResponse.getRequestParameter(questionId));
	}

	static void doStatRebuild() {
		final ITableEntityManager tMgr = QuestionUtils.applicationModule.getDataObjectManager(QuestionBean.class);
		//更新评论和分数
		final StringBuffer sql1 = new StringBuffer();
		sql1.append("update ").append(QuestionApplicationModule.question.getName()).append(" q set attentions=(select count(id) from ");
		sql1.append(QuestionApplicationModule.question_attention.getName()).append(" where questionId=q.id)");
		tMgr.execute(new SQLValue(sql1.toString()));
		sql1.setLength(0);
		sql1.append("update ").append(QuestionApplicationModule.question.getName()).append(" q set remarks=(select count(id) from ");
		sql1.append("it_question_remark qr").append(" where documentid=q.id and qr.parentid=0)");
		tMgr.execute(new SQLValue(sql1.toString()));
	}

	public static final IDataObjectQuery<?> queryQuestion(final PageRequestResponse requestResponse, final String type) {
		final ITableEntityManager tMgr = applicationModule.getDataObjectManager();
		Object userId = null;
		final List<Object> ol = new ArrayList<Object>();
		final StringBuffer sql = new StringBuffer();
		final StringBuffer where = new StringBuffer();
		if ("other".equals(type)) {
			final QuestionBean questionBean = applicationModule.getViewQuestionBean(requestResponse);
			if (questionBean != null) {
				userId = questionBean.getUserId();
				where.append(" and q.id<>").append(questionBean.getId());
			}
		} else if ("activity".equals(type)) {
			sql.append("select userId,count(id) as counter from ");
			sql.append(QuestionApplicationModule.question.getName()).append(" q ");
			sql.append(" group by userId ORDER BY counter desc");
			return tMgr.query(new SQLValue(sql.toString(), null));
		} else if ("os".equals(type)) {
			sql.append("SELECT o.shortTitle,o.id,count(*) as counter  FROM ");
			sql.append(QuestionApplicationModule.question_project.getName()).append(" qp left join ");
			sql.append("it_os o on qp.projectId=o.id where qp.projectId=o.id group by o.id");
			return tMgr.query(new SQLValue(sql.toString(), null));
		}
		sql.append("select q.* from ");
		sql.append(QuestionApplicationModule.question.getName()).append(" q ");
		if (userId != null) {
			where.append(" and q.userId=?");
			ol.add(userId);
		}
		if ("ref".equals(type)) {
			final String projectId = requestResponse.getRequestParameter("projectId");
			sql.append(" left join ").append(QuestionApplicationModule.question_project.getName()).append(" qp on");
			sql.append(" q.id=qp.questionId ");
			where.append(" and qp.projectId=?");
			ol.add(projectId);
		}
		sql.append("where 1=1 ").append(where);
		if ("time".equals(type)) {
			sql.append(" order by modifyDate desc");
		}
		tMgr.reset();
		return tMgr.query(new SQLValue(sql.toString(), ol.toArray(new Object[] {})), QuestionBean.class);
	}

	/**
	 * 查看问答共用多少积分
	 * @return
	 */
	public static long queryQuestionPoints(final PageRequestResponse requestResponse) {
		return applicationModule.getValue("select sum(reward) from it_question where userId=? and limitTime>?", new Object[] {
				ItSiteUtil.getLoginUser(requestResponse).getId(), new Date() });
	}

	public static String wrapOpenLink(final PageRequestResponse requestResponse, final QuestionBean questionBean) {
		final StringBuffer buf = new StringBuffer();
		if (questionBean != null) {
			buf.append("<a target=\"_blank\"");
			buf.append(" href=\"" + applicationModule.getViewUrl(questionBean.getId()) + "\"");
			buf.append(">").append(questionBean.getTitle()).append("</a>");
		}
		return buf.toString();
	}

	public static String formatQuestionHeader(final PageRequestResponse requestResponse, final QuestionBean questionBean) {
		final StringBuffer buf = new StringBuffer();
		buf.append("悬赏:<img style=\"vertical-align:center;\" src=\"").append(QuestionUtils.deploy).append("/score.gif\"> ");
		buf.append("<span class=\"_red\">").append(questionBean.getReward()).append("</span>(积分),<span style='color:#A41C05;font-weight: bold;'>");
		if (questionBean.getStatus() == EQuestionStatus.noraml) {
			buf.append("离问题结束还有");
			buf.append(DateUtils.onLineTimeInfo(questionBean.getLimitTime().getTime(), "{0,number,integer}天{1,number,integer}小时"));
		} else {
			buf.append(questionBean.getStatus().toString());
		}
		buf.append("</span>");
		buf.append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;by ").append(ItSiteUtil.layoutTime(requestResponse, questionBean, "yyyy-MM-dd HH", false));
		return buf.toString();
	}

}
