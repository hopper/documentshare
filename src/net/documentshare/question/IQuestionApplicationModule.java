package net.documentshare.question;

import net.documentshare.i.IItSiteApplicationModule;
import net.simpleframework.ado.db.IQueryEntitySet;
import net.simpleframework.web.page.PageRequestResponse;

public interface IQuestionApplicationModule extends IItSiteApplicationModule {

	/**
	* 显示开源软件时调用的方法，增加阅读量
	* 
	* @param requestResponse
	* @return
	*/
	QuestionBean getViewQuestionBean(PageRequestResponse requestResponse);

	IQueryEntitySet<QuestionProjectBean> queryQuestionProjectBeans(final Object questionId);

}
