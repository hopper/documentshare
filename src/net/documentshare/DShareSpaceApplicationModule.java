package net.documentshare;

import java.util.ArrayList;

import net.simpleframework.content.bbs.BbsUtils;
import net.simpleframework.content.blog.BlogUtils;
import net.simpleframework.content.component.topicpager.ETopicQuery;
import net.simpleframework.my.friends.FriendsUtils;
import net.simpleframework.my.home.MyHomeUtils;
import net.simpleframework.my.message.MessageUtils;
import net.simpleframework.my.space.DefaultSpaceApplicationModule;
import net.simpleframework.my.space.MySpaceUtils;
import net.simpleframework.organization.IUser;
import net.simpleframework.organization.account.AccountSession;
import net.simpleframework.organization.account.IAccount;
import net.simpleframework.util.LocaleI18n;
import net.simpleframework.web.IWebApplicationModule;
import net.simpleframework.web.WebUtils;
import net.simpleframework.web.page.PageRequestResponse;
import net.simpleframework.web.page.component.ui.tabs.EMatchMethod;
import net.simpleframework.web.page.component.ui.tabs.TabHref;
import net.simpleframework.web.page.component.ui.tabs.TabsUtils;

public class DShareSpaceApplicationModule extends DefaultSpaceApplicationModule {
	@Override
	public String getSpaceUrl(final PageRequestResponse requestResponse, final IUser user) {
		final StringBuilder sb = new StringBuilder();
		sb.append("/space/").append(user.getId()).append(".html");
		return sb.toString();
	}

	@Override
	public String getMySpaceNavigationHtml(PageRequestResponse requestResponse) {
		final IAccount login = AccountSession.getLogin(requestResponse.getSession());
		final IUser user = login != null ? login.user() : null;
		if (user == null) {
			return "";
		}
		final StringBuilder sb = new StringBuilder();
		sb.append("<table style=\"width: 100%;\" cellspacing=\"5\" class=\"space_navigation\">");
		// col 1
		sb.append("<tr><td valign=\"top\" width=\"49%\"><div>");
		sb.append(MySpaceUtils.buildSpaceLink(requestResponse, "space.png", "/space/" + user.getId() + ".html?t=space", "我的空间"));
		sb.append("</div><div>");
		sb.append(MySpaceUtils.buildSpaceLink(requestResponse, "docu.png", "/space/" + user.getId() + ".html?t=document", "我的文档"));
		sb.append("</div><div>");
		sb.append(MySpaceUtils.buildSpaceLink(requestResponse, "corpus.png", "/space/" + user.getId() + ".html?t=corpus", "我的文辑"));
		sb.append("</div><div>");
		sb.append(MySpaceUtils.buildSpaceLink(requestResponse, "portal.png", MyHomeUtils.applicationModule));
		sb.append("</div>");
		sb.append("<div>");
		sb.append("</td>");
		// col 2
		sb.append("<td width=\"1px\" style=\"border-left: 3px double #aaa;\"></td>");
		// col 3
		sb.append("<td valign=\"top\"><div>");
		final String blogUrl = BlogUtils.applicationModule.getBlogUrl(requestResponse, user);
		sb.append("<a class=\"a2\" href=\"").append(requestResponse.wrapContextPath(WebUtils.addParameters(blogUrl, "op_act=add")))
				.append("\" style=\"float:right\">").append(LocaleI18n.getMessage("DefaultSpaceApplicationModule.5")).append("</a>");
		sb.append(MySpaceUtils.buildSpaceLink(requestResponse, "blog.gif", blogUrl, BlogUtils.applicationModule.getApplicationText()));
		sb.append("</div><div>");
//		sb.append(MySpaceUtils.buildSpaceLink(requestResponse, "forum.gif",
//				BbsUtils.applicationModule.getTopicUrl2(requestResponse, user, ETopicQuery.onlyTopic),
//				BbsUtils.applicationModule.getApplicationText()));
//		sb.append("</div><div>");
		sb.append(MySpaceUtils.buildSpaceLink(requestResponse, "friends.png", FriendsUtils.applicationModule));
		sb.append("</div><div>");
		sb.append(MySpaceUtils.buildSpaceLink(requestResponse, "message.png", MessageUtils.applicationModule));
		sb.append("</div></td></tr></table>");
		return sb.toString();
	}

	@Override
	public String tabs(final PageRequestResponse requestResponse) {
		final ArrayList<TabHref> al = new ArrayList<TabHref>();
		final IUser user = getAccount(requestResponse).user();
		final String spaceUrl = getSpaceUrl(requestResponse, user);
		if (user != null) {
			al.add(new TabHref("文档", spaceUrl));
			al.add(new TabHref("文辑", WebUtils.addParameters(spaceUrl, "t=corpus")));
			al.add(new TabHref("#(DefaultSpaceApplicationModule.1)", WebUtils.addParameters(spaceUrl, "t=space")));
			al.add(new TabHref(BlogUtils.applicationModule.getApplicationText(), WebUtils.addParameters(spaceUrl, "t=blog")));
			final TabHref bbsTab = new TabHref(BbsUtils.applicationModule.getApplicationText(), WebUtils.addParameters(spaceUrl, "t=bbs"));
			bbsTab.setMatchMethod(EMatchMethod.startsWith);
			al.add(bbsTab);
		}
		if (isMyAccount(requestResponse)) {
			al.add(createTabHref(requestResponse, MyHomeUtils.applicationModule));
			al.add(createTabHref(requestResponse, FriendsUtils.applicationModule));
			al.add(createTabHref(requestResponse, MessageUtils.applicationModule));
		}
		return TabsUtils.tabs(requestResponse, al.toArray(new TabHref[al.size()]));
	}

	private TabHref createTabHref(final PageRequestResponse requestResponse, final IWebApplicationModule applicationModule) {
		return new TabHref(applicationModule.getApplicationText(), applicationModule.getApplicationUrl(requestResponse));
	}

}
