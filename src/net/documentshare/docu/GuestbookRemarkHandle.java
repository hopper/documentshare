package net.documentshare.docu;

import java.util.ArrayList;
import java.util.Map;

import net.simpleframework.ado.db.ExpressionValue;
import net.simpleframework.ado.db.IQueryEntitySet;
import net.simpleframework.ado.db.ITableEntityManager;
import net.simpleframework.content.component.remark.DefaultRemarkHandle;
import net.simpleframework.core.IApplicationModule;
import net.simpleframework.core.ado.IDataObjectQuery;
import net.simpleframework.core.ado.db.Table;
import net.simpleframework.core.bean.IDataObjectBean;
import net.simpleframework.core.bean.IIdBeanAware;
import net.simpleframework.organization.OrgUtils;
import net.simpleframework.util.ConvertUtils;
import net.simpleframework.util.StringUtils;
import net.simpleframework.web.page.component.ComponentParameter;

public class GuestbookRemarkHandle extends DefaultRemarkHandle {

	@Override
	public Object getBeanProperty(ComponentParameter compParameter, String beanProperty) {
		if ("title".equals(beanProperty)) {
			return "投诉 (共 ${count} 条投诉)";
		}
		return super.getBeanProperty(compParameter, beanProperty);
	}

	public IApplicationModule getApplicationModule() {
		return DocuUtils.applicationModule;
	}

	@Override
	public Class<? extends IIdBeanAware> getEntityBeanClass() {
		return GuestbookRemark.class;
	}

	public <T extends IDataObjectBean> void doAddCallback(ComponentParameter compParameter, ITableEntityManager temgr, T t,
			java.util.Map<String, Object> data, java.lang.Class<T> beanClazz) {
		GuestbookRemark guestbookRemark = (GuestbookRemark) t;
		if (guestbookRemark != null) {
			GuestbookRemark temp = new GuestbookRemark();
			temp.setParentId(guestbookRemark.getId());
			temp.setDocumentId(guestbookRemark.getDocumentId());
			temp.setId(temgr.nextId("id"));
			temp.setIp(compParameter.request.getRemoteHost());
			temp.setUserId(OrgUtils.um().getUserByName("admin").getId());
			temp.setContent("我们会尽快处理您的投诉,谢谢！");
			temgr.insert(temp);
		}
	}

	public <T extends IDataObjectBean> void doBeforeAdd(ComponentParameter compParameter, ITableEntityManager temgr, T t,
			java.util.Map<String, Object> data, java.lang.Class<T> beanClazz) {
		super.doBeforeAdd(compParameter, temgr, t, data, beanClazz);
		GuestbookRemark guestbookRemark = (GuestbookRemark) t;
		guestbookRemark.setDocuId(ConvertUtils.toLong(compParameter.getRequestParameter("docuId"), 0));
		final DocuBean docuBean = DocuUtils.applicationModule.getBean(DocuBean.class, guestbookRemark.getDocuId());
		if (docuBean != null) {
			guestbookRemark.setContent("<p>我要投诉《" + DocuUtils.wrapOpenLink(compParameter, docuBean) + "》</p>" + guestbookRemark.getContent());
		}
	}

	@Override
	protected ExpressionValue getBeansSQL(ComponentParameter compParameter, Object parentId) {
		final ArrayList<Object> al = new ArrayList<Object>();
		final StringBuilder sql = new StringBuilder();
		final Object documentId = getDocumentId(compParameter);
		if (documentId == null) {
			sql.append(Table.nullExpr(getTableEntityManager(compParameter).getTable(), "documentid"));
		} else {
			sql.append("documentid=?");
			al.add(documentId);
		}
		sql.append(" and ");
		if (parentId == null) {
			sql.append(Table.nullExpr(getTableEntityManager(compParameter).getTable(), "parentid"));
		} else {
			sql.append("parentid=?");
			al.add(parentId);
		}
		return new ExpressionValue(sql.toString(), al.toArray());
	}

	protected <T extends IIdBeanAware> IDataObjectQuery<T> beans(final ComponentParameter compParameter, final Object parentId,
			final String orderBySQL) {
		@SuppressWarnings("unchecked")
		final Class<T> t = (Class<T>) getEntityBeanClass();
		final ExpressionValue ev = getBeansSQL(compParameter, parentId);
		final IQueryEntitySet<T> set;
		if (StringUtils.hasText(orderBySQL)) {
			set = getTableEntityManager(compParameter, t).query(new ExpressionValue(ev.getExpression() + " " + orderBySQL, ev.getValues()), t);
		} else {
			set = getTableEntityManager(compParameter, t).query(ev, t);
		}
		set.getQueryDialect().setCountSQL(ev);
		return set;
	}

	@Override
	public Map<String, Object> getFormParameters(ComponentParameter compParameter) {
		final Map<String, Object> parameters = super.getFormParameters(compParameter);
		putParameter(compParameter, parameters, DocuUtils.docuId);
		return parameters;
	}
}
