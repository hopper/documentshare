package net.documentshare.docu;

import net.documentshare.i.IItSiteApplicationModule;
import net.simpleframework.ado.db.IQueryEntitySet;
import net.simpleframework.ado.lucene.AbstractLuceneManager;
import net.simpleframework.core.bean.ITreeBeanAware;
import net.simpleframework.web.page.PageRequestResponse;
import net.simpleframework.web.page.component.ComponentParameter;

public interface IDocuApplicationModule extends IItSiteApplicationModule {

	/**
	* 增加阅读量
	* 
	* @param requestResponse
	* @return
	*/
	DocuBean getViewDocuBean(PageRequestResponse requestResponse);

	/**
	 * 获得相应的分类目录
	 */
	IQueryEntitySet<DocuCatalog> queryCatalogs(final PageRequestResponse requestResponse, ITreeBeanAware parent);

	/**
	* 获得相应的分类目录
	*/
	IQueryEntitySet<DocuCatalog> queryCatalogs(final Object catalogId);

	/**
	 * 获得相应的分类目录
	 */
	IQueryEntitySet<DocuCodeCatalog> queryCodeCatalogs(final Object catalogId);

	/**
	 * 创建文集搜索
	 * @param compParameter
	 * @return
	 */
	public AbstractLuceneManager createCorpusLuceneManager(final ComponentParameter compParameter);
}
