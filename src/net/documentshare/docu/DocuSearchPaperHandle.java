package net.documentshare.docu;

import java.util.Map;

import net.simpleframework.core.ado.IDataObjectQuery;
import net.simpleframework.util.HTMLBuilder;
import net.simpleframework.web.WebUtils;
import net.simpleframework.web.page.component.ComponentParameter;
import net.simpleframework.web.page.component.ui.pager.AbstractPagerHandle;

public class DocuSearchPaperHandle extends AbstractPagerHandle {
	@Override
	public Object getBeanProperty(final ComponentParameter compParameter, final String beanProperty) {
		if ("title".equals(beanProperty)) {
			final StringBuffer title = new StringBuffer();
			title.append("文档搜索").append(HTMLBuilder.NAV).append("所有结果");
			return title.toString();
		}
		return super.getBeanProperty(compParameter, beanProperty);
	}

	@Override
	public IDataObjectQuery<?> createDataObjectQuery(final ComponentParameter compParameter) {
		//文件索引暂且不考虑
		final String c = WebUtils.toLocaleString(compParameter.getRequestParameter("c"));
		return DocuUtils.applicationModule.createLuceneManager(compParameter).getLuceneQuery(c);
	}

	@Override
	public Map<String, Object> getFormParameters(ComponentParameter compParameter) {
		final Map<String, Object> parameters = super.getFormParameters(compParameter);
		putParameter(compParameter, parameters, "docu_");
		putParameter(compParameter, parameters, "_search_topic");
		putParameter(compParameter, parameters, "c");
		return parameters;
	}

}
