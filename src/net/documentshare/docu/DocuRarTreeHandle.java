package net.documentshare.docu;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import net.documentshare.documentconfig.DocumentConfigMgr;
import net.documentshare.documentconfig.StorageBean;
import net.simpleframework.util.StringUtils;
import net.simpleframework.web.page.component.ComponentParameter;
import net.simpleframework.web.page.component.ui.tree.AbstractTreeBean;
import net.simpleframework.web.page.component.ui.tree.AbstractTreeHandle;
import net.simpleframework.web.page.component.ui.tree.AbstractTreeNode;
import net.simpleframework.web.page.component.ui.tree.TreeNode;

public class DocuRarTreeHandle extends AbstractTreeHandle {

	final static List<String> exceptCode = new ArrayList<String>();
	public static final String[] docuExts = { "doc", "docx", "ppt", "pptx", "xls", "xlsx", "pdf", "wps", "xml", "txt" };
	public static final String[] videoExts = { "mp4", "flv" };
	public static final String[] rarExts = { "rar", "zip" };
	static {
		exceptCode.add("RAR");
		exceptCode.add("ZIP");
		exceptCode.add("SWF");
		exceptCode.add("FLV");
		exceptCode.add("MP4");
		exceptCode.add("EXE");
		exceptCode.add("COM");
		exceptCode.add("DLL");
		exceptCode.add("PDF");
		exceptCode.add("DOC");
		exceptCode.add("DOCX");
		exceptCode.add("PPT");
		exceptCode.add("PPTX");
		exceptCode.add("XLS");
		exceptCode.add("XLSX");
		exceptCode.add("WPS");
		exceptCode.add("CLASS");
		exceptCode.add("DB");
		exceptCode.add("DAT");
		exceptCode.add("MP3");
		exceptCode.add("WAV");
		exceptCode.add("WMA");
		exceptCode.add("MID");
		exceptCode.add("JPG");
		exceptCode.add("JPEG");
		exceptCode.add("BMP");
		exceptCode.add("GIF");
		exceptCode.add("ICO");
		exceptCode.add("ICON");
		exceptCode.add("PNG");
		exceptCode.add("CHM");
		exceptCode.add("ISO");
		exceptCode.add("TMP");
		exceptCode.add("TTF");
		exceptCode.add("DEX");
		exceptCode.add("APK");
		exceptCode.add("JAR");
		exceptCode.add("DLL");
		exceptCode.add("RTF");
		exceptCode.add("CPL");
		exceptCode.add("CAT");
		exceptCode.add("INF");
	}

	@Override
	public Collection<? extends AbstractTreeNode> getTreenodes(final ComponentParameter compParameter, final AbstractTreeNode treeNode) {
		final Collection<AbstractTreeNode> nodeList = new ArrayList<AbstractTreeNode>();
		final AbstractTreeBean treeBean = (AbstractTreeBean) compParameter.componentBean;
		if (treeNode == null) {
			final DocuBean docuBean = DocuUtils.applicationModule.getViewDocuBean(compParameter);
			if (docuBean != null) {
				if ("rar".equalsIgnoreCase(docuBean.getExtension()) || "zip".equalsIgnoreCase(docuBean.getExtension())) {
					final String path1 = DocuUtils.getDatabase(docuBean.getUserId());
					final StorageBean storageBean = DocumentConfigMgr.getDocuMgr().getStorageMap().get(docuBean.getPath2());
					if (storageBean != null) {
						final Map<String, Boolean> fileMap = storageBean.listFiles(path1 + docuBean.getId());
						for (final String fileName : fileMap.keySet()) {
							final TreeNode treeNode2 = new TreeNode(treeBean, treeNode, fileName);
							treeNode2.setText(fileName);
							if (docuBean.getDocuFunction() == EDocuFunction.data || docuBean.getDocuFunction() == EDocuFunction.tool) {
								treeNode2.setOpened(true);
							}
							if (fileMap.get(fileName) == true) {
								treeNode2.setOpened(true);
								treeNode2.setId(path1 + docuBean.getId() + "/" + fileName);
							} else {
								if (docuBean.getDocuFunction() == EDocuFunction.code) {
									final String extension = StringUtils.getFilenameExtension(fileName).toUpperCase();
									if (!exceptCode.contains(extension)) {
										treeNode2.setTooltip("双击文件显示代码");
										treeNode2.setJsDblclickCallback("showCode('" + docuBean.getId() + "','" + path1 + docuBean.getId() + "/"
												+ fileName + "');");
									}
								}
							}
							treeNode2.setAttribute("opened", treeNode2.isOpened());
							treeNode2.setAttribute("key", docuBean.getPath2());
							if (docuBean.getDocuFunction() == EDocuFunction.code)
								treeNode2.setAttribute("docuId", docuBean.getId());
							nodeList.add(treeNode2);
						}
					}
				}
			}
		} else {
			if (StringUtils.hasText(treeNode.getId()) && treeNode.getId().startsWith("/")) {
				final StorageBean storageBean = DocumentConfigMgr.getDocuMgr().getStorageMap().get((String) treeNode.getAttribute("key"));
				if (storageBean != null) {
					final Map<String, Boolean> fileMap = storageBean.listFiles(treeNode.getId());
					for (final String fileName : fileMap.keySet()) {
						final TreeNode treeNode2 = new TreeNode(treeBean, treeNode, fileName);
						treeNode2.setText(fileName);
						treeNode2.setAttribute("opened", (Boolean)treeNode.getAttribute("opened"));
						if (fileMap.get(fileName) == true) {
							treeNode2.setAttribute("key", treeNode.getAttribute("key"));
							treeNode2.setId(treeNode.getId() + "/" + fileName);
							treeNode2.setAttribute("docuId", treeNode.getAttribute("docuId"));
						} else {
							if (treeNode.getAttribute("docuId") != null) {
								final String extension = StringUtils.getFilenameExtension(fileName).toUpperCase();
								if (!exceptCode.contains(extension)) {
									treeNode2.setTooltip("双击文件显示代码");
									treeNode2.setJsDblclickCallback("showCode('" + treeNode.getAttribute("docuId") + "','"
											+ (treeNode.getId() + "/" + fileName) + "');");
								}
							}
						}
						nodeList.add(treeNode2);
					}
				}
			}
		}
		return nodeList;
	}
}
