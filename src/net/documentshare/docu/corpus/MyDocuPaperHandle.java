package net.documentshare.docu.corpus;

import java.util.HashMap;
import java.util.Map;

import net.documentshare.docu.DocuBean;
import net.documentshare.docu.DocuUtils;
import net.simpleframework.ado.db.ITableEntityManager;
import net.simpleframework.ado.db.SQLValue;
import net.simpleframework.core.ado.IDataObjectQuery;
import net.simpleframework.util.StringUtils;
import net.simpleframework.web.WebUtils;
import net.simpleframework.web.page.component.ComponentParameter;
import net.simpleframework.web.page.component.ui.pager.AbstractPagerHandle;

public class MyDocuPaperHandle extends AbstractPagerHandle {
	@Override
	public IDataObjectQuery<?> createDataObjectQuery(final ComponentParameter compParameter) {
		final String corpusId = WebUtils.toLocaleString(compParameter.getRequestParameter("corpusId"));
		final ITableEntityManager tMgr = DocuUtils.applicationModule.getDataObjectManager();
		StringBuffer sql = new StringBuffer(
				"select d.* from it_docu_documentshare d,it_docu_corpus c,it_docu_corpusofdoc cd where c.id=? and c.id=cd.CorpusId and cd.docId=d.id");
		return tMgr.query(new SQLValue(sql.toString(), new Object[] { corpusId }), DocuBean.class);
	}

	@Override
	public Map<String, Object> getFormParameters(ComponentParameter compParameter) {
		// TODO Auto-generated method stub
		Map<String, Object> param = new HashMap<String, Object>();
		final String corpusId = WebUtils.toLocaleString(compParameter.getRequestParameter("corpusId"));
		param.put("corpusId", StringUtils.hasText(corpusId) ? corpusId : "");
		return param;
	}
}
