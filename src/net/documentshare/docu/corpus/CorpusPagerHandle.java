package net.documentshare.docu.corpus;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import net.documentshare.common.CommonUtils;
import net.documentshare.docu.DocuUtils;
import net.documentshare.utils.ItSiteUtil;
import net.simpleframework.content.EContentType;
import net.simpleframework.core.ado.IDataObjectQuery;
import net.simpleframework.util.ConvertUtils;
import net.simpleframework.util.StringUtils;
import net.simpleframework.web.WebUtils;
import net.simpleframework.web.page.component.ComponentParameter;
import net.simpleframework.web.page.component.ui.pager.AbstractPagerHandle;

public class CorpusPagerHandle extends AbstractPagerHandle {

	@Override
	public IDataObjectQuery<?> createDataObjectQuery(ComponentParameter compParameter) {
		final String c = WebUtils.toLocaleString(compParameter.getRequestParameter("c"));
		if (StringUtils.hasText(c)) {
			return DocuUtils.applicationModule.createCorpusLuceneManager(compParameter).getLuceneQuery(c);
		}
		final int t = ConvertUtils.toInt(compParameter.getRequestParameter("t"), 0);
		List<Object> p = new ArrayList<Object>();
		if (t == 2) {
			p.add(EContentType.recommended);
		}
		return DocuUtils.applicationModule.queryBean(whereSql(compParameter), p.toArray(), CorpusBean.class);
	}

	@Override
	public Object getBeanProperty(ComponentParameter compParameter, String beanProperty) {
		if ("title".equals(beanProperty)) {
			final int o = ConvertUtils.toInt(compParameter.getRequestParameter("o"), 0);
			if (ItSiteUtil.isManage(compParameter, CommonUtils.applicationModule) && o == 1) {
				final StringBuffer buf = new StringBuffer();
				buf.append("<a onclick=\"$IT.A('updateCorpusWindow');\" id='myUpload'>创建文辑</a>");
				return buf.toString();
			}
		}
		return super.getBeanProperty(compParameter, beanProperty);
	}

	public String whereSql(ComponentParameter compParameter) {
		final int t = ConvertUtils.toInt(compParameter.getRequestParameter("t"), 0);
		String corpusTile = compParameter.getRequestParameter("corpusTile");
		String corpusClass = compParameter.getRequestParameter("_corpus_catalog");
		StringBuffer exp = new StringBuffer("1=1");
		if (StringUtils.hasText(corpusTile)) {
			exp.append(" and name like '%" + corpusTile + "%' ");
		}
		if (StringUtils.hasText(corpusClass) && exp.length() == 0) {
			exp.append(" and catalogId =" + corpusClass);
		} else if (StringUtils.hasText(corpusClass)) {
			exp.append(" and catalogId =" + corpusClass);
		}
		if (t == 2 && exp.length() == 0) {
			exp.append("  and contentType =? ");
		} else if (t == 2) {
			exp.append(" and contentType =? ");
		}
		if (t == 0) {
			exp.append(" order by createDate desc");
		}
		if (t == 1) {
			exp.append(" order by readTime desc");
		}
		if (t == 3) {
			exp.append(" order by attentions desc");
		}
		return exp.toString();
	}

	@Override
	public Map<String, Object> getFormParameters(ComponentParameter compParameter) {
		Map<String, Object> parameters = super.getFormParameters(compParameter);
		putParameter(compParameter, parameters, "corpusTile");
		putParameter(compParameter, parameters, "_corpus_catalog");
		putParameter(compParameter, parameters, "t");
		putParameter(compParameter, parameters, "o");
		return parameters;
	}
}
