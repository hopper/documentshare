package net.documentshare.docu.corpus;

import java.util.Date;
import java.util.List;
import java.util.Map;

import net.documentshare.docu.DocuUtils;
import net.simpleframework.ado.IDataObjectValue;
import net.simpleframework.ado.db.ExpressionValue;
import net.simpleframework.ado.db.ITableEntityManager;
import net.simpleframework.ado.db.event.TableEntityAdapter;
import net.simpleframework.core.id.ID;
import net.simpleframework.organization.IJob;
import net.simpleframework.web.page.IForward;
import net.simpleframework.web.page.component.ComponentParameter;
import net.simpleframework.web.page.component.base.ajaxrequest.AbstractAjaxRequestHandle;

public class CorpusOfDocAajxHandle extends AbstractAjaxRequestHandle {
	@Override
	public Object getBeanProperty(ComponentParameter compParameter, String beanProperty) {
		if ("jobExecute".equals(beanProperty)) {
			return IJob.sj_account_normal;
		}
		return super.getBeanProperty(compParameter, beanProperty);
	}

	public IForward addDocIntoCopus(final ComponentParameter compParameter) {
		return jsonForward(compParameter, new JsonCallback() {

			@Override
			public void doAction(Map<String, Object> json) throws Exception {
				final String docId = compParameter.getRequestParameter("docId");
				final String copusId = compParameter.getRequestParameter("corpusId");
				CorpusOfDocBean bean = new CorpusOfDocBean();
				bean.setCorpusId(ID.Utils.newID(Integer.parseInt(copusId)));
				bean.setDocId(ID.Utils.newID(Integer.parseInt(docId)));
				bean.setCreateDate(new Date());
				DocuUtils.applicationModule.doUpdate(bean, new TableEntityAdapter() {

					@Override
					public void afterInsert(ITableEntityManager manager, Object[] objects) {
						CorpusBean bean = DocuUtils.applicationModule.getBean(CorpusBean.class, copusId);
						if (Long.parseLong(bean.getFrontCover().getValue().toString()) == 0) {
							bean.setFrontCover(ID.Utils.newID(Integer.parseInt(docId)));
							DocuUtils.applicationModule.getDataObjectManager(CorpusBean.class).update(new Object[] { "frontCover" }, bean);
						}
					}

				});
			}
		});
	}

	public IForward deleteDocIntoCopus(final ComponentParameter compParameter) {
		return jsonForward(compParameter, new JsonCallback() {
			@Override
			public void doAction(Map<String, Object> json) throws Exception {
				final String docId = compParameter.getRequestParameter("docId");
				final String copusId = compParameter.getRequestParameter("corpusId");
				DocuUtils.applicationModule.getDataObjectManager(CorpusOfDocBean.class).deleteTransaction(
						new ExpressionValue("corpusId=" + copusId + " and docId=" + docId), new TableEntityAdapter() {
							@Override
							public void afterDelete(ITableEntityManager manager, IDataObjectValue dataObjectValue) {
								// TODO Auto-generated method stub
								CorpusBean corpus = DocuUtils.applicationModule.getBean(CorpusBean.class, copusId);
								List<Map<String, String>> data = null;
								if (Long.parseLong(corpus.getFrontCover().getValue().toString()) == Long.parseLong(docId)) {
									data = CorpusUtils.queryBycorpusId(corpus, 1);
									if (data.size() > 0) {
										corpus.setFrontCover(ID.Utils.newID(data.get(0).get("docId"), long.class));
									} else {
										corpus.setFrontCover(ID.zero);
									}
									DocuUtils.applicationModule.getDataObjectManager(CorpusBean.class).update(new Object[] { "frontCover" }, corpus);
								}
							}

						});
			}
		});
	}
}
