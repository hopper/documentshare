package net.documentshare.documentconfig;

import java.util.Map;

import net.simpleframework.organization.IJob;
import net.simpleframework.util.ConvertUtils;
import net.simpleframework.util.StringUtils;
import net.simpleframework.web.page.IForward;
import net.simpleframework.web.page.component.ComponentParameter;
import net.simpleframework.web.page.component.base.ajaxrequest.AbstractAjaxRequestHandle;

public class DocumentConfigAjaxHandle extends AbstractAjaxRequestHandle {

	@Override
	public Object getBeanProperty(ComponentParameter compParameter, String beanProperty) {
		if ("jobExecute".equals(beanProperty)) {
			return IJob.sj_manager;
		}
		return super.getBeanProperty(compParameter, beanProperty);
	}

	public IForward saveLocalStorage(final ComponentParameter compParameter) {
		return jsonForward(compParameter, new JsonCallback() {

			@Override
			public void doAction(final Map<String, Object> json) throws Exception {
				final StorageBean storageBean = DocumentConfigMgr.getDocuMgr().getStorageMap().get(compParameter.getRequestParameter("id_"));
				final String d_ = compParameter.getRequestParameter("d_");
				LocalStorageBean localStorageBean = null;
				if (storageBean == null) {
					localStorageBean = (LocalStorageBean) DocumentConfigMgr.getDocuMgr().createStorageBean("local");
				} else {
					localStorageBean = (LocalStorageBean) storageBean;
				}
				if (localStorageBean != null) {
					localStorageBean.setPath(d_);
				}
				if (localStorageBean.isDirectoryExists()) {
					DocumentConfigMgr.getDocuMgr().saveDocuMgr();
					json.put("act", "true");
				} else {
					DocumentConfigMgr.getDocuMgr().removeStorageBean(localStorageBean.getId());
					json.put("act", "false");
				}
			}
		});
	}

	public IForward saveFtpStorage(final ComponentParameter compParameter) {
		return jsonForward(compParameter, new JsonCallback() {

			@Override
			public void doAction(final Map<String, Object> json) throws Exception {
				final StorageBean storageBean = DocumentConfigMgr.getDocuMgr().getStorageMap().get(compParameter.getRequestParameter("id_"));
				final String host_ = compParameter.getRequestParameter("host_");
				final String port_ = compParameter.getRequestParameter("port_");
				final String user_ = compParameter.getRequestParameter("user_");
				final String pass_ = compParameter.getRequestParameter("pass_");
				final String url_ = compParameter.getRequestParameter("url_");
				final String path_ = compParameter.getRequestParameter("path_");
				FtpStorageBean ftpStorageBean = null;
				if (storageBean == null) {
					ftpStorageBean = (FtpStorageBean) DocumentConfigMgr.getDocuMgr().createStorageBean("ftp");
				} else {
					ftpStorageBean = (FtpStorageBean) storageBean;
				}
				if (ftpStorageBean != null) {

					ftpStorageBean.setHost(host_);
					ftpStorageBean.setPort(ConvertUtils.toInt(port_, 21));
					ftpStorageBean.setUser(user_);
					ftpStorageBean.setPass(pass_);
					ftpStorageBean.setUrl(url_);
					ftpStorageBean.setPath(path_);
				}
				if (ftpStorageBean.isDirectoryExists()) {
					json.put("act", "true");
					DocumentConfigMgr.getDocuMgr().saveDocuMgr();
				} else {
					DocumentConfigMgr.getDocuMgr().removeStorageBean(ftpStorageBean.getId());
					json.put("act", "false");
				}
			}
		});
	}

	public IForward deleteStorage(final ComponentParameter compParameter) {
		return jsonForward(compParameter, new JsonCallback() {

			@Override
			public void doAction(final Map<String, Object> json) throws Exception {
				if (!DocumentConfigMgr.getDocuMgr().removeStorageBean(compParameter.getRequestParameter("id"))) {
					json.put("act", "已经有数据，无法删除！");
				}
			}
		});
	}

	public IForward currentStorage(final ComponentParameter compParameter) {
		return jsonForward(compParameter, new JsonCallback() {

			@Override
			public void doAction(final Map<String, Object> json) throws Exception {
				DocumentConfigMgr.getDocuMgr().currentStorage(compParameter.getRequestParameter("id"));
			}
		});
	}

	public IForward valueSave(final ComponentParameter compParameter) {
		return jsonForward(compParameter, new JsonCallback() {

			@Override
			public void doAction(final Map<String, Object> json) throws Exception {
				final String rar = compParameter.getRequestParameter("rar");
				if (StringUtils.hasText(rar)) {
					DocumentConfigMgr.getDocuMgr().putValue("rar", rar);
					DocumentConfigMgr.getDocuMgr().saveDocuMgr();
				}
			}
		});
	}
}
