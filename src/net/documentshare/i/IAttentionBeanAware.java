/**
 * 
 */
package net.documentshare.i;

/**
 * @author 李岩飞
 *
 */
public interface IAttentionBeanAware {

	long getAttentions();

	void setAttentions(long attentions);
}
