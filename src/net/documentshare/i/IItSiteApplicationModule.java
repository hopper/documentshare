package net.documentshare.i;

import java.util.List;
import java.util.Map;

import net.documentshare.impl.AbstractAttention;
import net.simpleframework.ado.db.IQueryEntitySet;
import net.simpleframework.ado.db.ITableEntityManager;
import net.simpleframework.ado.db.event.ITableEntityListener;
import net.simpleframework.ado.lucene.AbstractLuceneManager;
import net.simpleframework.content.IContentApplicationModule;
import net.simpleframework.core.bean.AbstractIdDataObjectBean;
import net.simpleframework.web.page.PageRequestResponse;
import net.simpleframework.web.page.component.ComponentParameter;

public interface IItSiteApplicationModule extends IContentApplicationModule {
	void doUpdate(final String sql);

	void doUpdate(final List<? extends AbstractIdDataObjectBean> beanList);

	void doUpdate(final AbstractIdDataObjectBean bean, ITableEntityListener tel);

	void doUpdate(final AbstractIdDataObjectBean bean);

	void doDelete(final AbstractIdDataObjectBean bean, ITableEntityListener tel);

	void doDelete(final AbstractIdDataObjectBean bean);

	void doDelete(final List<? extends AbstractIdDataObjectBean> beanList);

	void doDelete(final List<? extends Object> idList, Class<? extends Object> beanClass);

	void doDelete(final Class<?> bean, final Object id, ITableEntityListener tel);

	void doDelete(final Class<?> bean, final Object id);

	void doDelete(final String where, final Class<?> bean);

	void doDelete(final PageRequestResponse requestResponse);

	void doUpdate(final Object[] columns, final Object bean);

	void doUpdate(final Object[] columns, final Object bean, ITableEntityListener tel);

	IQueryEntitySet<Map<String, Object>> queryBean(final String sql, final Object[] values);

	<T> IQueryEntitySet<T> queryBean(final String exp, Class<T> t);

	<T> IQueryEntitySet<T> queryBean(final String exp, final Object[] values, Class<T> t);

	<T> List<T> doQuery(final String sql, final String col, Class<T> t);

	<T> T getBean(final Class<T> t, final Object id);

	<T> T getBeanByExp(final Class<T> t, final String exp, final Object[] params);

	String getDeployPath();

	long getValue(final String sql, Object[] values);

	AbstractLuceneManager createLuceneManager(final ComponentParameter compParameter);

	String tabs13(PageRequestResponse requestResponse);

	void deleteAttentions(final Object documentId);

	AbstractAttention getAttention(final Object userId, final Object id);

	String getViewUrl(Object id);

	ITableEntityManager getDataObjectManager(final Class<?> t);

	ITableEntityManager getDataObjectManager();
}
