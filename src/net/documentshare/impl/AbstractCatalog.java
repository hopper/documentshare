package net.documentshare.impl;

import net.documentshare.i.IItSiteApplicationModule;
import net.simpleframework.ado.db.IEntityBeanAware;
import net.simpleframework.ado.db.IQueryEntitySet;
import net.simpleframework.ado.db.ITableEntityManager;
import net.simpleframework.ado.db.event.TableEntityAdapter;
import net.simpleframework.content.component.catalog.Catalog;
import net.simpleframework.core.bean.ITreeBeanAware;
import net.simpleframework.util.HTMLBuilder;

/**
 * @author 李岩飞
 *
 */
public abstract class AbstractCatalog extends Catalog implements IEntityBeanAware {

	private int counter;//该类目下的项目数

	public void setCounter(int counter) {
		this.counter = counter;
	}

	public int getCounter() {
		return counter;
	}

	public ITreeBeanAware parent(final IItSiteApplicationModule applicationModule) {
		return applicationModule.getBean(getClass(), getParentId());
	}

	public static class Utils {
		public static String buildCatalog(final Object id, final Class<? extends AbstractCatalog> classBean,
				final IItSiteApplicationModule applicationModule, final boolean url) {
			return buildCatalog(id, classBean, applicationModule, url, null);
		}

		public static String buildCatalogTitle(final Object id, final Class<? extends AbstractCatalog> classBean,
				final IItSiteApplicationModule applicationModule) {
			final StringBuffer buf = new StringBuffer();
			final AbstractCatalog catalog = applicationModule.getBean(classBean, id);
			buildCatalogTitle(catalog, applicationModule, buf);
			buf.append(catalog.getText());
			return buf.toString();
		}

		public static void buildCatalogTitle(final AbstractCatalog catalog, final IItSiteApplicationModule applicationModule, final StringBuffer buf) {
			if (catalog == null)
				return;
			final AbstractCatalog pCatalog = (AbstractCatalog) catalog.parent(applicationModule);
			if (pCatalog != null) {
				buildCatalogTitle(pCatalog, applicationModule, buf);
				buf.append(pCatalog.getText()).append(HTMLBuilder.NAV);
			}
		}

		public static String buildCatalog(final Object id, final Class<? extends AbstractCatalog> classBean,
				final IItSiteApplicationModule applicationModule, final boolean url, final String param) {
			final AbstractCatalog catalog = applicationModule.getBean(classBean, id);
			final StringBuffer buf = new StringBuffer();
			if (catalog == null)
				return buf.toString();
			if (catalog != null) {
				final AbstractCatalog pCatalog = (AbstractCatalog) catalog.parent(applicationModule);
				if (pCatalog != null)
					buildCatalog(pCatalog, applicationModule, buf, url, param);
				if (url) {
					final String appUrl = applicationModule.getApplicationUrl(null);
					buf.append("<a href='").append(appUrl).append("?");
					if (param != null) {
						buf.append(param);
						if (pCatalog == null)
							buf.append("&catalogPId=");
						else
							buf.append("&catalogId=");
					} else {
						if (pCatalog == null)
							buf.append("&catalogPId=");
						else
							buf.append("&catalogId=");
					}
					buf.append(catalog.getId());
					buf.append("'>").append(catalog.getText()).append("</a>");
				} else
					buf.append(catalog.getText());
				buf.append(HTMLBuilder.NAV);
			}
			return buf.toString();
		}

		private static void buildCatalog(final AbstractCatalog catalog, final IItSiteApplicationModule applicationModule, final StringBuffer buf,
				final boolean url, final String param) {
			if (url) {
				final String appUrl = applicationModule.getApplicationUrl(null);
				buf.append("<a href='").append(appUrl).append("?");
				if (param != null) {
					buf.append(param).append("&catalogPId=");
				} else {
					buf.append("catalogPId=");
				}
				buf.append(catalog.getId()).append("'>").append(catalog.getText()).append("</a>");
			} else
				buf.append(catalog.getText());
			buf.append(HTMLBuilder.NAV);
		}

		/**
		 * 更新counter
		 */
		public static void updateCatalog(final Object id, final Class<? extends AbstractCatalog> classBean,
				final IItSiteApplicationModule applicationModule, final boolean add) {
			final AbstractCatalog catalog = applicationModule.getBean(classBean, id);
			if (catalog != null)
				updateCatalog(catalog, applicationModule, add);
		}

		/**
		 * 更新counter
		 */
		public static void updateCatalog(final AbstractCatalog catalog, final IItSiteApplicationModule applicationModule, final boolean add) {
			synchronized (applicationModule) {
				catalog.setCounter(catalog.getCounter() + (add ? 1 : -1));
				final AbstractCatalog pCatalog = (AbstractCatalog) catalog.parent(applicationModule);
				if (pCatalog == null) {
					applicationModule.doUpdate(new Object[] { "counter" }, catalog);
				} else {
					applicationModule.doUpdate(new Object[] { "counter" }, catalog, new TableEntityAdapter() {
						@Override
						public void afterUpdate(ITableEntityManager manager, Object[] objects) {
							updateParentCatalog(pCatalog, applicationModule, add);
						}
					});
				}
			}
		}

		private static void updateParentCatalog(final AbstractCatalog catalog, final IItSiteApplicationModule applicationModule, final boolean add) {
			catalog.setCounter(catalog.getCounter() + (add ? 1 : -1));
			final AbstractCatalog pCatalog = (AbstractCatalog) catalog.parent(applicationModule);
			if (pCatalog == null) {
				applicationModule.doUpdate(new Object[] { "counter" }, catalog);
			} else {
				applicationModule.doUpdate(new Object[] { "counter" }, catalog, new TableEntityAdapter() {
					@Override
					public void afterUpdate(ITableEntityManager manager, Object[] objects) {
						updateParentCatalog(pCatalog, applicationModule, add);
					}
				});
			}
		}

		public static void statCounter(final Class<? extends AbstractCatalog> classBean, final IItSiteApplicationModule applicationModule) {
			synchronized (applicationModule) {
				final IQueryEntitySet<? extends AbstractCatalog> qs = applicationModule.queryBean("parentId<>0", classBean);
				if (qs != null) {
					AbstractCatalog catalog = null;
					while ((catalog = qs.next()) != null) {
						final StringBuffer sql = new StringBuffer();
						sql.append("SELECT SUM(COUNTER) FROM ");
						sql.append(applicationModule.getDataObjectManager(classBean).getTablename());
						sql.append(" WHERE parentId=?");
						final long value = applicationModule.getValue(sql.toString(), new Object[] { catalog.getId() });
						if (value != 0) {
							catalog.setCounter((int) value);
							applicationModule.doUpdate(catalog);
						}
					}
				}
			}
		}
	}

}
